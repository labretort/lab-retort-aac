# LabRetort AAC #



### What is LabRetort AAC? ###

**LabRetort AAC** (**A**ugmentative and **A**lternative **C**ommunication) is an accessible **Azure AI** interface / chatbot 
that generates lab report PDFs and accesses **STEM** curricula.